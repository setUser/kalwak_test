addEventListener("load", () => {
    window.app = new Vue({
        delimiters: ['[[', ']]'],
        el: '#root',
        data: {
            isValid: false,
            file: null,
            uploadtext: 'Upload',
            isuploading: false,
        },
        watch: {
            file: {
                immediate: true,
                handler() {
                    this.check()
                }
            }
        },
        methods: {
            check() {
                if (this.file) {
                    let name = this.file.name
                    let type = this.file.type
                    let ext = name.substring(name.indexOf('.') + 1, name.length)
                    if (type === 'application/pdf' && ext === 'pdf') {
                        this.isValid = true
                    } else {
                        this.isValid = false
                    }
                }
            },
            upload() {
                this.isuploading = true
                this.isValid = false
                this.uploadtext = 'Uploading...'
                this.file.text().then(data => {
                    fetch('', {
                        method: 'post',
                        body: JSON.stringify({ name: this.file.name, blob: data })
                    }).then(() => {
                        this.isuploading = false
                        this.uploadtext = 'Upload'
                        this.file = null
                    })
                })
            },
        },
    })
});
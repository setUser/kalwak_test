from django.db import models

# Create your models here.
class PDF_Files(models.Model) :
    name = models.CharField(max_length=256)
    blob = models.BinaryField()

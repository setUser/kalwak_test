from django.contrib import admin

# Register your models here.
from .models import PDF_Files

admin.site.register(PDF_Files)
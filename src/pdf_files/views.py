from django.shortcuts import render
from .models import PDF_Files
import json


# Create your views here.


def index(request, *args, **kwargs):
    if request.method == 'POST':
        body = json.loads(request.body)
        PDF_Files.objects.create(
            name=body.get('name'),
            blob=body.get('blob').encode('utf-8')
        )
    return render(request, 'index.html', {})